class DecimalTranslator
  DICTIONARY = {
    1 => 'I',
    5 => 'V',
    10 => 'X',
    50 => 'L',
    100 => 'C',
    500 => 'D',
    1000 => 'M'
  }

  def translate(decimal)
    power = decimal.to_s.length - 1
    nearest_decimal = decimal + 10**power

    if roman = DICTIONARY[decimal]
      roman
    elsif roman = DICTIONARY[nearest_decimal]
      DICTIONARY[10**power] + roman
    end
  end
end
