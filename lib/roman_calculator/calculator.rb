class Calculator
  DICTIONARY = {
    1 => 'I',
    5 => 'V',
    10 => 'X',
    50 => 'L',
    100 => 'C',
    500 => 'D',
    1000 => 'M'
  }

  def reverse_dictionary
    DICTIONARY.invert
  end

  def add(roman1, roman2)
    if roman1 == roman2
      roman1 + roman2
    elsif ending = roman2.slice(/#{ roman1 }$/, 0)
      starting = roman2.slice(/^(.+)#{ roman1 }/, 1)
      starting + DICTIONARY[reverse_dictionary[ending] + reverse_dictionary[ending]]
    else
      roman1 + roman2
    end
  end
end
