require 'support/test_helper'

describe "RomanCalculator" do
  it "must add I + I" do
    calculator = Calculator.new
    calculator.add('I', 'I').must_equal 'II'
  end

  it "must add V + IV" do
    calculator = Calculator.new
    calculator.add('V', 'IV').must_equal 'IX'
  end

  it "must add V + II" do
    calculator = Calculator.new
    calculator.add('V', 'II').must_equal 'VII'
  end
end
