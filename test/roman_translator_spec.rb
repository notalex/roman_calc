require 'support/test_helper'

describe "DecimalTranslator" do
  it "must translate 5 into equivalent roman" do
    translator = DecimalTranslator.new
    translator.translate(5).must_equal 'V'
  end

  it "must translate 4 into equivalent roman" do
    translator = DecimalTranslator.new
    translator.translate(4).must_equal 'IV'
  end

  it "must translate 9 into equivalent roman" do
    translator = DecimalTranslator.new
    translator.translate(9).must_equal 'IX'
  end

  it "must translate 40 into equivalent roman" do
    translator = DecimalTranslator.new
    translator.translate(40).must_equal 'XL'
  end

  it "must translate 900 into equivalent roman" do
    translator = DecimalTranslator.new
    translator.translate(900).must_equal 'CM'
  end
end
